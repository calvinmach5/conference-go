from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(query):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={query}"
    response = requests.get(url, headers=headers)
    api_dict = response.json()
    if "photos" in api_dict and api_dict["photos"]:
        picture = {"picture_url": api_dict["photos"][0]["src"]["originial"]}
        return {"picture": [picture]}
    else:
        return {"picture": None}


def get_weather_data(city, state):
    geo_url = f"https://api.openweathermap.org/data/2.5/data/2.5/onecall"
    geocoding_params = {
        "q": f"{city},{state}",
        "appid": OPEN_WEATHER_API_KEY,
    }
    geocoding_response = requests.get(geo_url, params=geocoding_params)
    geocoding_data = geocoding_response.json()

    if geocoding_response.status_code != 200:
        return {"weather": None}
    latitude = geocoding_data["coord"]["lat"]
    longitude = geocoding_data["cord"]["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/onecall"
    weather_params = {
        "lat": latitude,
        "lon": longitude,
        "exclude": "minutely, hourly, daily",
        "appid": OPEN_WEATHER_API_KEY,
    }
    weather_response = requests.get(weather_url, params=weather_params)
    weather_data = weather_response.json()

    if weather_response.status_code != 200:
        return {"weather": None}

    main_temp = weather_data["current"]["temp"]
    weather_description = weather_data["current"]["weather"][0]["description"]
    return {
        "weather": {"tempature": main_temp, "description": weather_description}
    }
